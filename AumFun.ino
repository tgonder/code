#include <AcceleroMMA7361.h>

int ledPinOut = 8;
int pbIn = 7;                 

AcceleroMMA7361 accelero;

int x;
int y;
int z;
int sum;
int pbState;

volatile long ct = 0;
volatile int state = 0;

void setup()
{
  Serial.begin(9600);

  pinMode(ledPinOut, OUTPUT);
  pinMode(pbIn, INPUT);

  accelero.begin(13, 12, 11, 10, A0, A1, A2);
  accelero.setARefVoltage(5);                   //sets the AREF voltage to 3.3V
  accelero.setSensitivity(LOW);                   //sets the sensitivity to +/-6G
  accelero.calibrate();
  Serial.println("");
  Serial.println("Hit the push button to start logging...");

  ct = 0;
}

void loop()
{
  pbState = digitalRead(pbIn);
  if (pbState == LOW) {
    state = !state;
    digitalWrite(ledPinOut, state);

    if (state == 0) { 
      ct = 0;
      Serial.println("");
      Serial.println("");
    } 
    
    // give a half-second delay to prevent long button pushes
    delay(500);
  }
  
  if (state == 0) {
    // don't do anything since we hit are paused
    return;
  }
  
  x = accelero.getXRaw();
  y = accelero.getYRaw();
  z = accelero.getZRaw();
  
  sum = x + y + z;
  
  //Serial.println("");
  Serial.print(ct);
  Serial.print(",");
  Serial.print(x);
  Serial.print(",");
  Serial.print(y);
  Serial.print(",");
  Serial.print(z);
  Serial.print(",");
  Serial.print(sum);
  Serial.println(",");
  //delay(50);   
  ct++;
}



